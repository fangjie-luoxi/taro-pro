### 使用说明

- 1 拉取依赖

```
tyarn
```

- 2 运行项目

```
# 微信小程序
yarn dev:weapp
# h5
yarn dev:h5
```

- 3 打包项目

```
# 小程序
yarn build:weapp
# h5
yarn build:h5
```

### 根据数据库结构生成代码(需要配置根目录下的 `lgmake.yaml` 文件)

```
// 生成代码命令
lgmake taro gen
```

### git 提交规范

```
feat: 添加功能
fix: 修改bug
docs: 修改文档
UI: 样式修改
```

### src目录结构
